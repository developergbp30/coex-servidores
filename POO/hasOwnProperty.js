let nombres = {
    nombre: "Juan",
    apellido: "Pérez"
}
console.log(nombres.nombre);
console.log(nombres.hasOwnProperty("segNombre"));
console.log(nombres);
// Resultado esperado:
// Juan
// false