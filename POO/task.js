const _MS_PER_DAY = 1000 * 60 * 60 * 24;
class Task {
    
    constructor(id, title, description, status, startDate, endDate){
        this.id = id;
        this.title = title;
        this.description = description;
        this.status = status;
        this.startDate =  startDate;
        this.endDate =  endDate;
    }
    done(){
        this.status=true;
    }
    reject(){
        this.status=false;
    }

    dateDiffInDays() {
        // Discard the time and time-zone information.
        const utc1 = Date.UTC(this.startDate.getFullYear(), this.startDate.getMonth(), this.startDate.getDate());
        const utc2 = Date.UTC(this.endDate.getFullYear(), this.endDate.getMonth(), this.endDate.getDate());
      
        return Math.floor((utc2 - utc1) / _MS_PER_DAY);
      }
//Todo: Determinar si la tarea se completo, esta a tiempo, o no se completo.
    statusMessage() {
            let diff =  this.dateDiffInDays();
            console.log(diff);
            if(diff > 0 && this.status ==  false){
                console.log("Aun estas a Tiempo");
                return "Aun estas a Tiempo";
            }
            
            if(diff < 0 && this. status ==  false){
                console.log("¡Ups, tarea no terminada :( !");
                return "¡Ups, tarea no terminada :( !";
            }

            if(diff < 0 && this. status ==  true){
                console.log("¡Tarea completada!");
                return "¡Tarea completada!";
            }
    }
}
let compras = new Task(1, "frutas", "compra de frutas", false, new Date("2022-02-09"), new Date("2022-02-08"));

compras.statusMessage();