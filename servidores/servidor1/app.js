const express = require('express')
const app = express()
const port = 3000

app.use(express.json());

const tasks = [
    {   id: 1,
        title: "Crear la ruta get /task",
        status: true,
        date: "2022/02/03"
    }
];

app.get('/', (req, res) => {
  res.send('Estamos en COEX primer server')
})

app.get('/task', (req, res)=> {

    res.send(tasks);
})

app.post('/task', (req, res)=> {
    console.log(req.body);
    tasks.push(req.body);
    res.send(req.body);
})

app.put('/task/:id', (req, res) => {
  const { body } =  req;
  const id =  req.params.id;
  console.log(body);
 const find =  tasks.findIndex((i)=> i.id == id)
 if(find < 0){
   return res.send({message: "Registro no encotrado"});
 }
 /* tasks[find] = {
    id: id,
    title:    body.title,
    status :  body.status,
    date:     body.date
 } */

 tasks[find].title =  body.title;
 tasks[find].status =  body.status;
 tasks[find].date =  body.date;
  res.send(tasks[find]);
})

app.delete('/task/:id', (req, res) => {
  const id =  req.params.id;
 const find =  tasks.findIndex((i)=> i.id == id)
 if(find < 0){
   return res.send({message: "Registro no encontrado"});
 }
 tasks.splice(find, 1);
  res.send(tasks);
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})